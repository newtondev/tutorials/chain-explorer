import { Injectable } from '@nestjs/common';
import { BlockFrostAPI, Responses} from '@blockfrost/blockfrost-js';

@Injectable()
export class AppService {
    API = new BlockFrostAPI({
      projectId: 'preprod13i6zFr5ExyfF19tgMIN9D99ihTFykaX'
    })
  async getHello(blockHash: string): Promise<Responses['block_content']> {
    const block = await this.API.blocks(blockHash);
    //const networkInfo = await API.network();
    //const latestEpoch = await API.epochsLatest();
    //const health = await API.health();
    //const address = await API.addresses(
      //"addr1qxqs59lphg8g6qndelq8xwqn60ag3aeyfcp33c2kdp46a09re5df3pzwwmyq946axfcejy5n4x0y99wqpgtp2gd0k09qsgy6pz"
    //);
    //const pools = await API.pools({ page: 1, count: 10, order: "asc" });
    return block;
  }

  async getBlock(blockHash: string): Promise<Responses['block_content']> {
    return await this.API.blocks(blockHash)
  }

  async getTransaction(transactionHash: string): Promise<Responses['tx_content']> {
    return await this.API.txs(transactionHash)
  }

  async getAddress(address: string): Promise<Responses['address_content']> {
    return await this.API.addresses(address)
  }

  async getStakeKey(stakeKey: string): Promise<Responses['account_content']> {
    return await this.API.accounts(stakeKey)
  }
}

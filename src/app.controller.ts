import { Controller, Get, Query } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/data')
  public async index(@Query() params: {blockHash: string}) {
    const result = await this.appService.getHello(params.blockHash)
    return { message: result };
  }

  @Get('/blocks')
  public async getBlock(@Query() params: {blockHash: string}) {
    const result = await this.appService.getBlock(params.blockHash)
    return { message: result };
  }

  @Get('/transactions')
  public async getTransaction(@Query() params: {transactionHash: string}) {
    const result = await this.appService.getTransaction(params.transactionHash)
    return { message: result };
  }
  @Get('/addresses')
  public async getaddress(@Query() params: {address: string}) {
    const result = await this.appService.getAddress(params.address)
    return { message: result };
  }
  @Get('/stake-keys')
  public async getStakeKey(@Query() params: {stakeKey: string}) {
    const result = await this.appService.getStakeKey(params.stakeKey)
    return { message: result };
  }
}

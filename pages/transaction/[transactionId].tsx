import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { useState, useEffect} from "react"
import axios from 'axios'
import { Responses } from '@blockfrost/blockfrost-js'

const Transaction:NextPage = () => {
    const router = useRouter()
    const [transactionDetails, setTransactionDetails] = useState({} as Responses['tx_content'])
    const [hash, setHash ] = useState("")

    useEffect(() => {
       if (router.isReady) {
           const { transactionId } = router.query
           console.log("TRANSACTIONID", transactionId)
           if(transactionId) setHash(transactionId as string)
       }
    }, [router.isReady])

    useEffect(() => {
        const asyncFunc = async () => {
            if(hash) {
                //call server with search value
                const result = await axios.get(`http://localhost:3000/transactions?transactionHash=${hash}`).then((response) => {
                    console.log(response)
                    setTransactionDetails(response.data.message)
                })
                //set blockDetails with result
            }
        }
        asyncFunc()
    }, [hash])

    const getOutput = (txDetails: Responses['tx_content']) => {
        return (txDetails.output_amount && txDetails.output_amount[0]) ?
               `${txDetails.output_amount[0].quantity} ${txDetails.output_amount[0].unit}` :
               "null"
    }

    return (
        <>
            <div>
                <h3 className="text-lg font-medium leading-6 text-gray-900">Transaction Hash</h3>
                <p className="mt-1 max-w-2xl text-sm text-gray-500">{transactionDetails.hash}</p>
            </div>
            <div className="mt-5 border-t border-gray-200">
                <dl className="sm:divide-y sm:divide-gray-200">
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Block</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">{transactionDetails.block}</dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Block Height</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">{transactionDetails.block_height}</dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Block Time</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">{transactionDetails.block_time}</dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Slot</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">{transactionDetails.slot}</dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Output</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                    {getOutput(transactionDetails)}
                    </dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Total Fees</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">{transactionDetails.fees}</dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">UTXO Count</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                        {transactionDetails.utxo_count}
                    </dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Body Size</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                    {transactionDetails.size}
                    </dd>
                </div>
                </dl>
            </div>
        </>
    )
}
export default Transaction

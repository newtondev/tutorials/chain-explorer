import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { useState, useEffect} from "react"
import axios from 'axios'
import { Responses } from '@blockfrost/blockfrost-js'

const StakeKey:NextPage = () => {
    const router = useRouter()
    const [stakeKeyDetails, setStakeKeyDetails] = useState({} as Responses['account_content'])
    const [stakeKeyId, setStakeKeyId ] = useState("")

    useEffect(() => {
       if (router.isReady) {
           const { stakeKey } = router.query
           if(stakeKey) setStakeKeyId(stakeKey as string)
       }
    }, [router.isReady])

    useEffect(() => {
        const asyncFunc = async () => {
            if(stakeKeyId) {
                //call server with search value
                const result = await axios.get(`http://localhost:3000/stake-keys?stakeKey=${stakeKeyId}`).then((response) => {
                    console.log(response)
                    setStakeKeyDetails(response.data.message)
                })
                //set blockDetails with result
            }
        }
        asyncFunc()
    }, [stakeKeyId])

    return (
        <>
            <div>
                <h3 className="text-lg font-medium leading-6 text-gray-900">Stake Address</h3>
                <p className="mt-1 max-w-2xl text-sm text-gray-500">{stakeKeyDetails.stake_address}</p>
            </div>
            <div className="mt-5 border-t border-gray-200">
                <dl className="sm:divide-y sm:divide-gray-200">
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Active</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">{stakeKeyDetails.active}</dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Controlled Amount</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">{stakeKeyDetails.controlled_amount}</dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Rewards</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">{stakeKeyDetails.rewards_sum}</dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Withdrawals</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">{stakeKeyDetails.withdrawals_sum}</dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Reserves</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                        {stakeKeyDetails.reserves_sum}
                    </dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Treasury</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">{stakeKeyDetails.treasury_sum}</dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Withdrawable Amount</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                        {stakeKeyDetails.withdrawable_amount}
                    </dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Pool Id</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                    {stakeKeyDetails.pool_id}
                    </dd>
                </div>
                </dl>
            </div>
        </>
    )
}
export default StakeKey

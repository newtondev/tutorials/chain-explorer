import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { useState, useEffect} from "react"
import axios from 'axios'
import { Responses } from '@blockfrost/blockfrost-js'

const Address:NextPage = () => {
    const router = useRouter()
    const [addressDetails, setAddressDetails] = useState({} as Responses['address_content'])
    const [addressId, setAddressId ] = useState("")

    useEffect(() => {
       if (router.isReady) {
           const { address } = router.query
           if(address) setAddressId(address as string)
       }
    }, [router.isReady])

    useEffect(() => {
        const asyncFunc = async () => {
            if(addressId) {
                //call server with search value
                const result = await axios.get(`http://localhost:3000/addresses?address=${addressId}`).then((response) => {
                    console.log(response)
                    setAddressDetails(response.data.message)
                })
                //set blockDetails with result
            }
        }
        asyncFunc()
    }, [addressId])

    const getBalances = (addressDetails: Responses['address_content']) => {
        if (addressDetails.amount) {
            return addressDetails.amount.map(({quantity, unit}) => {
                return (
                    `${quantity} ${unit} \n`
                )
            })
        } else {
            return "empty"
        }
    }

    return (
        <>
            <div>
                <h3 className="text-lg font-medium leading-6 text-gray-900">Address</h3>
                <p className="mt-1 max-w-2xl text-sm text-gray-500">{addressDetails.address}</p>
            </div>
            <div className="mt-5 border-t border-gray-200">
                <dl className="sm:divide-y sm:divide-gray-200">
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Balances</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                        {getBalances(addressDetails)}
                    </dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Stake Address</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">{addressDetails.stake_address}</dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Script</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">{addressDetails.script}</dd>
                </div>
                </dl>
            </div>
        </>
    )
}
export default Address

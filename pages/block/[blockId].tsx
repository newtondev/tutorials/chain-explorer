import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { useState, useEffect} from "react"
import axios from 'axios'
import { Responses } from '@blockfrost/blockfrost-js'

const Block:NextPage = () => {
    const router = useRouter()
    const { blockId } = router.query
    const [blockDetails, setBlockDetails] = useState({} as Responses['block_content'])
    const [id, setId ] = useState("")

    useEffect(() => {
       if (router.isReady) {
           const { blockId } = router.query
           console.log("blockID", blockId)
           if(blockId) setId(blockId as string)
       }
    }, [router.isReady])

    useEffect(() => {
        const asyncFunc = async () => {
            if(blockId) {
                //call server with search value
                console.log('effect called')
                const result = await axios.get(`http://localhost:3000/data?blockHash=${id}`).then((response) => {
                    console.log(response)
                    setBlockDetails(response.data.message)
                })
                //set blockDetails with result
            }
        }
        asyncFunc()
    }, [id])

    return (
        <>
            <div>
                <h3 className="text-lg font-medium leading-6 text-gray-900">Block Details</h3>
                <p className="mt-1 max-w-2xl text-sm text-gray-500">{blockDetails.height}</p>
            </div>
            <div className="mt-5 border-t border-gray-200">
                <dl className="sm:divide-y sm:divide-gray-200">
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Transactions</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">{blockDetails.tx_count}</dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Epoch/Slot</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">{blockDetails.epoch}/{blockDetails.epoch_slot}</dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Absolute Slot</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">{blockDetails.slot}</dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Timestamp</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">{blockDetails.time}</dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Stake Pool / Slot Leader</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                    {blockDetails.slot_leader}
                    </dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Total Fees</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                    {blockDetails.fees}
                    </dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Total Output</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                    {blockDetails.output}
                    </dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Block Hash</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                    {blockDetails.hash}
                    </dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Body Size</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                    {blockDetails.size}
                    </dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Issuer VKey????</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                    a9d974fd26bfaf385749113f260271430276bed6ef4dad6968535de6778471ce
                    </dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">VRF Result</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">{blockDetails.block_vrf}</dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Broadcast Protocol Version???</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                    7.0
                    </dd>
                </div>
                <div className="py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:py-5">
                    <dt className="text-sm font-medium text-gray-500">Ledger Block Version???</dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                    5 (Alonzo)
                    </dd>
                </div>
                </dl>
            </div>
        </>
    )
}
export default Block

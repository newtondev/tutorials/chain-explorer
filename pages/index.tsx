import type { NextPage } from 'next'
import { useState, useCallback, useEffect} from "react"
import Head from 'next/head'
import Image from 'next/image'
import { Responses } from '@blockfrost/blockfrost-js'
import axios from 'axios'
import { PaperClipIcon } from '@heroicons/react/20/solid'

interface Body {
  message: Responses['block_content'];
}

interface Props {
  data: Body | null;
  error: string | null;
}


const Home: NextPage = ({ data, error }) => {
  const [blockDetails, setBlockDetails] = useState({})
  const [searchTerm, setSearchTerm] = useState("")

  useEffect(() => {
    const asyncFunc = async () => {
      //call server with search value
        console.log('effect called')
        const result = await axios.get(`http://localhost:3000/data?blockHash=${searchTerm}`).then((response) => {
          console.log(response)
          setBlockDetails(response.data.message)
        })
      //set blockDetails with result
    }
    asyncFunc()
  }, [searchTerm])

  const handleSubmit = useCallback((event) => {
    event.preventDefault()
    setSearchTerm(event.target[0].value)
  })

  return (
    <div className="flex min-h-screen flex-col items-center justify-center py-2">
      <Head>
        <title>Create Chain Explorer App</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="stylesheet" href="https://rsms.me/inter/inter.css" />
      </Head>

      <main className="flex w-full flex-1 flex-col items-center justify-center px-20 text-center">
        <h1 className="text-6xl font-bold">
          Welcome to{' '}
          <a className="text-blue-600" href="https://nextjs.org">
            Chain Explorer!
          </a>
        </h1>

        <div className="text-2xl">
          Get started by searching{' '}
          <code className="rounded-md bg-gray-100 p-3 font-mono text-lg">
            <div className="bg-white shadow sm:rounded-lg">
              <div className="px-4 py-5 sm:p-6">
                <h3 className="text-lg font-medium leading-6 text-gray-900">Search Pre-prod</h3>
                <div className="mt-2 max-w-xl text-sm text-gray-500">
                  <p>Search for transaction, epoch or block</p>
                </div>
                <form className="mt-5 sm:flex sm:items-center" onSubmit={handleSubmit}>
                  <div className="w-full sm:max-w-xs">
                    <label htmlFor="email" className="sr-only">
                      Enter id / hash
                    </label>
                    <input
                      type="search"
                      name="search"
                      id="search"
                      className="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                      placeholder="Search transaction, address, block, epoch.slot, pool, stakeKey, policyId.assetName, fingerprint, policyId"
                      ref={el => el ? setSearchTerm(el.value) : null}
                    />
                  </div>
                  <button
                    type="submit"
                    className="mt-3 inline-flex w-full items-center justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                  >
                    Search
                  </button>
                </form>
              </div>
            </div>
          </code>
        </div>

      </main>

      <footer className="flex h-24 w-full items-center justify-center border-t">
        <a
          className="flex items-center justify-center gap-2"
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <span>Newton</span>
        </a>
      </footer>
    </div>
  )
}

// https://nextjs.org/docs/basic-features/data-fetching#getstaticprops-static-generation
export const getStaticProps: GetStaticProps = async () => {
  let data: any | null = null;
  let error: string | null = null;

  try {
    const res = await fetch('http://localhost:3000/data');
      console.log("server data: ", res)

    if (res.ok) {
      data = await res.json();
    } else {
      error = res.statusText;
    }
  } catch (e: unknown) {
    error = (e as Error).message;
  }

  return { props: { data, error } };
};

export default Home;

# Chain Explorer

## Why a Chain Explorer?


## Getting started

1. Product Research
2. Tool Discovery
3. Project Setup
4. Front-end & blockfrost imntegration
5. Replace Blockfrost with native node

### Product Research
https://cardanoscan.io/
 * need to be able to explore the follownig on chain entities:
 Must haves:
    * Blocks - 1
    * Epochs - 1
    * Metadata
    * Stakekeys
    * Pools - 1
    * Tokens - 1
    * Transactions - 1
    * Addresses - 1
Nice To have features:
    * ada price
    * ada market cap
    * total staked
    * epoch count down
    * mempool / contract transactions
    * stake pool explorer

### Tool Research
1. Blockfrost.io
2. node & react
    * nextjs.org - Frontend Framework
        * Tailwindcss.com / ui component framework
    * nestjs.com - Backend Framework
3. Cardano Node and supporting tools & libraries
